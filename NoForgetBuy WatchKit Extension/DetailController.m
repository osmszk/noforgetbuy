//
//  DetailController.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2015/04/07.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "DetailController.h"


@interface DetailController()

@end


@implementation DetailController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
    
    NSString *con = (NSString*)context;
    [self.detailLabel setText:con];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



