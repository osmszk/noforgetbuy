//
//  InterfaceController.m
//  NoForgetBuy WatchKit Extension
//
//  Created by 鈴木治 on 2015/04/07.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController()

@property (weak, nonatomic) IBOutlet WKInterfaceMap *map;
@property (nonatomic) MKCoordinateRegion currentRegion;
@property (nonatomic) MKCoordinateSpan currentSpan;

@property (weak, nonatomic) IBOutlet WKInterfaceButton *inButton;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *outButton;

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *messageLabel;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    LOG_CURRENT_METHOD;
//    NSUserDefaults *d = [NSUserDefaults standardUserDefaults];
//    DEBUGLOG(@"%@",[d stringForKey:KEY_NOTIFI_MSG]);
    //WatchKitでiPhone Appとデータ共有する方法
    //ref:http://qiita.com/_tid_/items/dfff60ec8e93fefe1af4
    
    
    _currentSpan = MKCoordinateSpanMake(0.005f, 0.005f);
    
    
    
}

- (void)willActivate {
    NSLog(@"%@ will activate", self);

    [self zoomIn];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc]initWithSuiteName:GROUP_NAME];
    if ([defaults objectForKey:KEY_NOTIFI_LATITUDE]) {
        [self goToSettingPlace];
        [_messageLabel setText:@"この地点で通知されます。"];
    }else{
        _currentSpan = MKCoordinateSpanMake(0.01f, 0.01f);
        [self goToTokyo];
        [_messageLabel setText:@"位置情報がまだ登録されていません。通知してほしい地点を、アプリ上で登録してください。"];
    }
    
    [self.map addAnnotation:self.currentRegion.center withPinColor:WKInterfaceMapPinColorRed];
}
    
- (void)didDeactivate {
    // This method is called when the controller is no longer visible.
    NSLog(@"%@ did deactivate", self);
    
}

- (void)goToSettingPlace{
    NSUserDefaults *defaults = [[NSUserDefaults alloc]initWithSuiteName:GROUP_NAME];
    double lat = [defaults doubleForKey:KEY_NOTIFI_LATITUDE];
    double lon = [defaults doubleForKey:KEY_NOTIFI_LONGITUDE];
    
    NSLog(@"lat:%f",lat);
    NSLog(@"lon:%f",lon);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
    
    [self setMapToCoordinate:coordinate];
}

- (void)goToTokyo {
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(35.681382f, 139.766084f);
    
    [self setMapToCoordinate:coordinate];
}

- (void)setMapToCoordinate:(CLLocationCoordinate2D)coordinate {
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, self.currentSpan);
    self.currentRegion = region;
    
    MKMapPoint newCenterPoint = MKMapPointForCoordinate(coordinate);
    
    [self.map setVisibleMapRect:MKMapRectMake(newCenterPoint.x, newCenterPoint.y, self.currentSpan.latitudeDelta, self.currentSpan.longitudeDelta)];
    [self.map setRegion:region];
}

- (IBAction)zoomOut {
    MKCoordinateSpan span = MKCoordinateSpanMake(self.currentSpan.latitudeDelta * 2, self.currentSpan.longitudeDelta * 2);
    MKCoordinateRegion region = MKCoordinateRegionMake(self.currentRegion.center, span);
    
    self.currentSpan = span;
    [self.map setRegion:region];
}

- (IBAction)zoomIn {
    MKCoordinateSpan span = MKCoordinateSpanMake(self.currentSpan.latitudeDelta * 0.5f, self.currentSpan.longitudeDelta * 0.5f);
    MKCoordinateRegion region = MKCoordinateRegionMake(self.currentRegion.center, span);
    
    self.currentSpan = span;
    [self.map setRegion:region];
}

- (IBAction)addPinAnnotations {
    [self.map addAnnotation:self.currentRegion.center withPinColor:WKInterfaceMapPinColorRed];
    
    CLLocationCoordinate2D greenCoordinate = CLLocationCoordinate2DMake(self.currentRegion.center.latitude, self.currentRegion.center.longitude - 0.3f);
    [self.map addAnnotation:greenCoordinate withPinColor:WKInterfaceMapPinColorGreen];
    
    CLLocationCoordinate2D purpleCoordinate = CLLocationCoordinate2DMake(self.currentRegion.center.latitude, self.currentRegion.center.longitude + 0.3f);
    [self.map addAnnotation:purpleCoordinate withPinColor:WKInterfaceMapPinColorPurple];
}

- (IBAction)removeAll {
    [self.map removeAllAnnotations];
}
@end
