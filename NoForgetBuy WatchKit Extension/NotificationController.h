//
//  NotificationController.h
//  NoForgetBuy WatchKit Extension
//
//  Created by 鈴木治 on 2015/04/07.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
