//
//  PLNotificationHandler.h
//  NoForgetBuy
//
//  Created by 鈴木治 on 2015/01/03.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PLNotificationHandler : NSObject

+ (void)setNotificationWithTarget:(CLLocationCoordinate2D)target radius:(CLLocationDistance)radius msg:(NSString *)msg;
+ (void)registerSettingsAndCategories;
+ (void)cancelAllNotification;


@end
