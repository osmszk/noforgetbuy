//
//  PLInfoViewController.h
//  TokyoSento
//
//  Created by Osamu Suzuki on 2014/11/10.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
@protocol PLInfoViewControllerDelegate <NSObject>
- (void)infoViewClose:(PLInfoViewController *)viewController;
@end
*/

@interface PLInfoViewController : UITableViewController
//@property (nonatomic,assign) id<PLInfoViewControllerDelegate> delegate;
@property (weak,nonatomic) IBOutlet UILabel *radiusLabel;
@property (weak,nonatomic) IBOutlet UILabel *msgLabel;
@end
