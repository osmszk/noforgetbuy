//
//  PLMsgEditViewController.h
//  NoForgetBuy
//
//  Created by 鈴木治 on 2015/01/03.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PLMsgEditViewController;

//@protocol PLMsgEditViewControllerDelegate <NSObject>
//- (void)msgEditView:(PLMsgEditViewController *)controller finished:(NSString *)msg;
//@end

@interface PLMsgEditViewController : UIViewController

@property (weak,nonatomic) IBOutlet UITextField *textField;
//@property (assign,nonatomic) id<PLMsgEditViewControllerDelegate> delegate;

//- (IBAction)pushedDecideButton:(id)sender;

@end
