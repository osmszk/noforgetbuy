//
//  FirstViewController.h
//  NoForgetBuy
//
//  Created by 鈴木治 on 2014/12/26.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@class HTPressableButton;

@interface PLMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)pushedCurrentLocationButton:(id)sender;

@end

