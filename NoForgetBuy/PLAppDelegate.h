//
//  AppDelegate.h
//  NoForgetBuy
//
//  Created by 鈴木治 on 2014/12/26.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) id<GAITracker> tracker;

@end

