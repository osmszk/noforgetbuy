//
//  AppDelegate.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2014/12/26.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import "PLAppDelegate.h"
#import <CoreLocation/CoreLocation.h>

#import "AMoAdSDK.h"
#import "ImobileSdkAds/ImobileSdkAds.h"

@interface PLAppDelegate ()

@end

@implementation PLAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [AMoAdSDK initSDK];
    
    [ImobileSdkAds registerWithPublisherID:AD_IMOBILE_PUBLISHER_ID
                                   MediaID:AD_IMOBILE_MEDIA_ID
                                    SpotID:AD_IMOBILE_SPOT_ID_BANNER1];
    [ImobileSdkAds startBySpotID:AD_IMOBILE_SPOT_ID_BANNER1];
    
    [ImobileSdkAds registerWithPublisherID:AD_IMOBILE_PUBLISHER_ID
                                   MediaID:AD_IMOBILE_MEDIA_ID
                                    SpotID:AD_IMOBILE_SPOT_ID_ICON];
    [ImobileSdkAds startBySpotID:AD_IMOBILE_SPOT_ID_ICON];//アイコン型
    
#if ENABLE_ANALYTICS
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_TRACKING_ID];
    self.tracker = tracker;
#endif
    
    PLGAppVersionState appversionState = [PLUtil appVersionState];
    if (appversionState == PLGAppVersionStateFirst) {
        [PLUtil saveInteger:100 key:KEY_RADIUS_NOTIFI];//v1.0.0-
        [PLUtil saveObject:@"登録地点に着きました！" key:KEY_NOTIFI_MSG];//v1.0.0-
    }else if(appversionState == PLGAppVersionStateBumpedUp){
        
    }
    
    NSMutableSet *categories = [NSMutableSet set];
    UIMutableUserNotificationAction *notifyAction = [[UIMutableUserNotificationAction alloc] init];
    notifyAction.title = @"買い物を忘れない";
    notifyAction.identifier = @"notifyAction";
    notifyAction.activationMode = UIUserNotificationActivationModeForeground;
    notifyAction.authenticationRequired = NO;
    
    UIMutableUserNotificationCategory *buyCategory = [[UIMutableUserNotificationCategory alloc] init];
    [buyCategory setActions:@[notifyAction]
                     forContext:UIUserNotificationActionContextDefault];
    buyCategory.identifier = @"dontforget";
    
    [categories addObject:buyCategory];
    
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types
                                      categories:categories];
    [application registerUserNotificationSettings:settings];
    
    
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]) {
        [self handleLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
    }
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 
 
 
 
 */

#pragma mark - 



- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"%s - %@", __PRETTY_FUNCTION__, notification);
}

- (void)handleLocalNotification:(UILocalNotification *)notification {
    
    [[[UIAlertView alloc] initWithTitle:notification.alertBody message:nil delegate:nil cancelButtonTitle:notification.alertAction otherButtonTitles: nil] show];
    
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    
    NSLog(@"");
    
}


@end
