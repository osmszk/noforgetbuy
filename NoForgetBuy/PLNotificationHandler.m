//
//  PLNotificationHandler.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2015/01/03.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "PLNotificationHandler.h"

@implementation PLNotificationHandler

+ (void)setNotificationWithTarget:(CLLocationCoordinate2D)target radius:(CLLocationDistance)radius msg:(NSString *)msg
{
    if (!CLLocationCoordinate2DIsValid(target)) {
        DEBUGLOG(@"CLLocationCoordinate2DIsValid");
        return;
    }
    
    
    
//    CLLocationDistance radius_ = radius;  // 半径何メートルに入ったら通知するか
    NSString *identifier = @"identifier"; // 通知のID
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:target
                                                                 radius:radius
                                                             identifier:identifier];
    
    // Notificationセット
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = msg;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.alertAction = @"買い物を忘れない!";
    localNotification.regionTriggersOnce = YES;
    localNotification.region = region;
    localNotification.category = @"watch";
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc]initWithSuiteName:GROUP_NAME];
    [defaults setDouble:target.latitude forKey:KEY_NOTIFI_LATITUDE];
    [defaults setDouble:target.longitude forKey:KEY_NOTIFI_LONGITUDE];
}

+ (void)registerSettingsAndCategories {
    NSMutableSet *categories = [NSMutableSet set];
    
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    acceptAction.title = NSLocalizedString(@"Accept", @"Accept invitation");
    acceptAction.identifier = @"accept";
    acceptAction.activationMode = UIUserNotificationActivationModeBackground;
    acceptAction.authenticationRequired = NO;
    
    UIMutableUserNotificationAction *declineAction = [[UIMutableUserNotificationAction alloc] init];
    declineAction.title = NSLocalizedString(@"Decline", @"Decline invitation");
    declineAction.identifier = @"decline";
    declineAction.activationMode = UIUserNotificationActivationModeBackground;
    declineAction.authenticationRequired = NO;
    
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    [inviteCategory setActions:@[acceptAction, declineAction]
                    forContext:UIUserNotificationActionContextDefault];
    inviteCategory.identifier = @"invitation";
    [categories addObject:inviteCategory];
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                            (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound)
                                                                             categories:categories];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

+ (void)cancelAllNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc]initWithSuiteName:GROUP_NAME];
    [defaults removeObjectForKey:KEY_NOTIFI_LATITUDE];
    [defaults removeObjectForKey:KEY_NOTIFI_LONGITUDE];
}


@end
