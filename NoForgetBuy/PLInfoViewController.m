//
//  PLInfoViewController.m
//  TokyoSento
//
//  Created by Osamu Suzuki on 2014/11/10.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import "PLInfoViewController.h"
#import "PLSelectViewController.h"
#import "PLMsgEditViewController.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#include <sys/types.h>
#include <sys/sysctl.h>

#import <Social/Social.h>

#import "AMoAdSDK.h"

enum {
    PLActionSheetTagShareToFriend = 0,
};


@interface PLInfoViewController ()<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>{
    
    NSInteger _faqSection;//1
    NSInteger _tutorialSection;//1
    NSInteger _moreAppSection;//1
    NSInteger _contactSection;//2
    NSInteger _specialThanksSection;//2
    NSInteger _settingSection;//2
    NSInteger _friendSection;
    
    NSInteger _sectionCount;//==5
    
    BOOL _isShowingPicker;
    
    UIView *_textEditAllView;
}


@end

@implementation PLInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"情報";
    
    //UI
    self.navigationController.navigationBar.translucent = NAVI_BAR_TRANSLUCENT;
    
    _faqSection           = 0;
    _settingSection       = 1;
    _moreAppSection       = 2;
    _friendSection        = 3;
    _contactSection       = 4;
    _specialThanksSection = 5;
    
    _tutorialSection = -1;
    
    _sectionCount = 6;
    
    NSInteger r = [PLUtil loadInteger:KEY_RADIUS_NOTIFI];
    self.radiusLabel.text = [NSString stringWithFormat:@"%dm",(int)r];
    
    NSString *msg = [PLUtil loadObject:KEY_NOTIFI_MSG];
    self.msgLabel.text = msg;
    
    BOOL isReviewOver = [PLUtil loadBool:KEY_WALL_AD_SHOW_FLG];
    if (isReviewOver) {
        UIBarButtonItem *rightbarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"おすすめ" style:UIBarButtonItemStylePlain target:self action:@selector(pushedRecommendButton:)];
        self.navigationItem.rightBarButtonItem = rightbarButtonItem;
    }
    
}

- (void)pushedRecommendButton:(id)sender
{
    [self wallAd];
}

- (void)wallAd
{
    [AMoAdSDK showAppliPromotionWall:self
                         orientation:UIInterfaceOrientationPortrait
                     wallDrawSetting:APSDK_Ad_Key_WallDrawSetting_hiddenStatusBar
                              appKey:AD_APPLIPROMOTION_ID
                    onWallCloseBlock:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
#if ENABLE_ANALYTICS
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName value:@"Info Screen"];
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:animated];
    
    NSInteger r = [PLUtil loadInteger:KEY_RADIUS_NOTIFI];
    self.radiusLabel.text = [NSString stringWithFormat:@"%dm",(int)r];
    
    NSString *msg = [PLUtil loadObject:KEY_NOTIFI_MSG];
    self.msgLabel.text = msg;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Action

- (void)pushedCloseButton:(id)sender
{
//    [self.delegate infoViewClose:self];
}

- (void)pushedReviewButton:(id)sender
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE];
    [[UIApplication sharedApplication] openURL:url];
}

//- (IBAction)clockPreviewCellSwithed:(id)sender
//{
//    UISwitch *sw = (UISwitch*)sender;
//    DEBUGLOG(@"sw on:%d",sw.on);
//    [PLUtil saveBool:sw.on key:KEY_PREVIEW_SHOW_FLG];
//}

#pragma mark - Custom

- (void)moveToFaqView
{
    NSString *path = [NSString stringWithFormat:@"%@/faq.txt",[[NSBundle mainBundle]resourcePath]];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *string =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    
    UIViewController *textViewController = [[UIViewController alloc]init];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, [PLUtil displaySize].width, [PLUtil displaySize].height)];
    textView.editable = NO;
    if([PLUtil osVersion]>=7)textView.selectable = NO;
    textView.text = string;
    textView.font = [UIFont systemFontOfSize:14.0f];
    textViewController.view = textView;
    textViewController.title = @"よくある質問";
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (void)moveToSozaiView
{
    NSString *path = [NSString stringWithFormat:@"%@/sozai.txt",[[NSBundle mainBundle]resourcePath]];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *string =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    
    UIViewController *textViewController = [[UIViewController alloc]init];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, [PLUtil displaySize].width, [PLUtil displaySize].height)];
    textView.editable = NO;
    if([PLUtil osVersion]>=7)textView.selectable = NO;
    textView.text = string;
    textViewController.view = textView;
    textViewController.title = @"素材";
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (void)moveToLisenceView
{
    NSString *path = [NSString stringWithFormat:@"%@/license.txt",[[NSBundle mainBundle]resourcePath]];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *string =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    
    UIViewController *textViewController = [[UIViewController alloc]init];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, [PLUtil displaySize].width, [PLUtil displaySize].height)];
    textView.editable = NO;
    if([PLUtil osVersion]>=7)textView.selectable = NO;
    textView.text = string;
    textViewController.view = textView;
    textViewController.title = @"ライセンス表記";
    [self.navigationController pushViewController:textViewController animated:YES];
}

- (NSString *)devicePlatform
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine
                                            encoding:NSUTF8StringEncoding];
    free(machine);
    return platform;
}

- (void)openMailApp
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
        mailPicker.mailComposeDelegate = self;
        [mailPicker setToRecipients:[NSArray arrayWithObjects:SUPPORT_MAIL, nil]];
        
        UIDevice *device = [UIDevice currentDevice];
        NSMutableString *msg = [NSMutableString string];
        [msg appendString:@"\n\n\n---------\n"];
        [msg appendString:@"※今後の改修のため、お客様情報を送信します\n"];
        [msg appendString:[NSString stringWithFormat:@"Version:%@\n",[PLUtil appVersionString]]];
        [msg appendString:[NSString stringWithFormat:@"iOS:%@\n",[device systemVersion]]];
        [msg appendString:[NSString stringWithFormat:@"Device:%@\n",[self devicePlatform]]];
        [msg appendString:[NSString stringWithFormat:@"Screen:%@\n",NSStringFromCGSize([PLUtil displaySize])]];
        [mailPicker setSubject:[NSString stringWithFormat:@"%@ お問い合わせ/ご要望",APP_NAME]];
        [mailPicker setMessageBody:msg isHTML:NO];
        [self presentViewController:mailPicker animated:YES completion:nil];
    }else {
        
        UIAlertView *alertMail = [[UIAlertView alloc]
                                  initWithTitle:@"Information"
                                  message:@"No Email function"
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertMail show];
    }
}

- (void)openTwitter
{
    NSURL *url = [NSURL URLWithString:URL_SUPPORT_TWITTER];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStore
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfOyajiNeko
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_OYAJINEK];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfUnlocker
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_UNLOCKER];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfCalendar
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_CALENDER];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfMemo
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_MEMO];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfMessage
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_MESSAGE];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)moveToAppStoreOfAll
{
    NSURL *url = [NSURL URLWithString:URL_APP_STORE_ALL];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)shareToFriend
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"友達にこのアプリを教える"
                                  delegate:self
                                  cancelButtonTitle:@"キャンセル"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"LINE",
                                  @"Twitter",
                                  @"Facebook",
                                  nil];
    actionSheet.tag = PLActionSheetTagShareToFriend;
    [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)sendViaLine:(NSString *)text
{
    NSString *msgText = [PLUtil changeURLencode:text];
    NSString *urlString = [NSString stringWithFormat:@"line://msg/text/%@", msgText];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
#if ENABLE_ANALYTICS
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"conversion" action:@"share" label:@"line" value:nil] build]];
#endif
    } else {
        [PLUtil showAlert:@"エラー" message:@"LINE、利用できない><"];
    }
}

- (void)sendViaTwitter:(NSString *)text
{
    if(![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        //NSLocalizedString
        [PLUtil showAlert:@"" message:@"Twitter、利用できない！><"];
        return;
    }
    
    SLComposeViewController *tweetViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    NSString *msgText = [NSString stringWithFormat:@"%@ %@",text,HASH_TAG];
    [tweetViewController setInitialText:msgText];
    [tweetViewController addURL:[NSURL URLWithString:URL_APP_STORE]];
    
    tweetViewController.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            NSLog(@"Cancelled");
        } else{
            NSLog(@"Done");
#if ENABLE_ANALYTICS
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"conversion" action:@"share" label:@"twitter" value:nil] build]];
#endif
            
            //NSLocalizedString
            [PLUtil showAlert:@"" message:@"シェア成功！"];
        }
        
        [self dismissViewControllerAnimated:YES completion:Nil];
    };
    
    [self presentViewController:tweetViewController animated:YES completion:nil];
    
    
}

- (void)sendViaFacebook:(NSString *)text
{
    if(![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        //NSLocalizedString
        [PLUtil showAlert:@"" message:@"Facebook、利用できない！><"];
        return;
    }
    
    SLComposeViewController *tweetViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    NSString *msgText = [NSString stringWithFormat:@"%@",text];
    [tweetViewController setInitialText:msgText];
    [tweetViewController addURL:[NSURL URLWithString:URL_APP_STORE]];
    
    tweetViewController.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            NSLog(@"Cancelled");
        } else{
            NSLog(@"Done");
#if ENABLE_ANALYTICS
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"conversion" action:@"share" label:@"facebook" value:nil] build]];
#endif
            
            //NSLocalizedString
            [PLUtil showAlert:@"" message:@"シェア成功！"];
        }
        
        [self dismissViewControllerAnimated:YES completion:Nil];
    };
    
    [self presentViewController:tweetViewController animated:YES completion:nil];
    
    
}

- (void)moveToSelectView:(PLDSelectType)selectType
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PLSelectViewController *selectViewController = (PLSelectViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"selectview"];
    selectViewController.selectType = selectType;
    [self.navigationController pushViewController:selectViewController animated:YES];
}

- (void)moveToMsgEditView
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PLMsgEditViewController *selectViewController = (PLMsgEditViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"msgeditview"];
    [self.navigationController pushViewController:selectViewController animated:YES];
}

#pragma mark - UITableViewDataDelegate

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    if(indexPath.section == _specialThanksSection){
        if(indexPath.row==0){
            [self moveToSozaiView];
            return;
        }else{
            [self moveToLisenceView];
            return;
        }
    }else if(indexPath.section == _contactSection){
        if (indexPath.row == 0) {
            [self openMailApp];
        }else{
            [self openTwitter];
        }
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow]  animated:YES];
    }else if(indexPath.section == _tutorialSection){
        //        PLTutorialViewController *tutorialViewController = [[PLTutorialViewController alloc]initWithNibName:@"PLTutorialViewController" bundle:nil];
        //        tutorialViewController.disableProceed = YES;
        //        [self.navigationController pushViewController:tutorialViewController animated:YES];
    }else if(indexPath.section == _faqSection){
        [self moveToFaqView];
    }else if(indexPath.section == _moreAppSection){
        if (indexPath.row == 0) {
            [self moveToAppStoreOfMemo];
        }else if (indexPath.row == 1) {
            [self moveToAppStoreOfMessage];
        }else if(indexPath.row == 2){
            [self moveToAppStoreOfCalendar];
        }else{
            [self moveToAppStoreOfAll];
        }
    }else if (indexPath.section == _settingSection){
        
        if (indexPath.row == 0) {
            //半径
            [self moveToSelectView:PLDSelectTypeRadius];
        }else if (indexPath.row == 1){
            //通知テキスト
            if ([PLUtil loadBool:KEY_WALL_AD_SHOW_FLG]) {
                //Not in review
                if([PLUtil loadBool:KEY_ENABLE_CHANGE_NOTIFI_MSG]){
                    [self moveToMsgEditView];
                }else{
                    
                    UIAlertController *alertControler = [UIAlertController alertControllerWithTitle:@"" message:@"このアプリの良さをレビューで伝えて頂いた方に、「通知メッセージ編集」機能をプレゼント!\n星5つだと制作者として、とっても励みになります^^" preferredStyle:UIAlertControllerStyleAlert];
                    [alertControler addAction:[UIAlertAction actionWithTitle:@"また今度" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                        
                    }]];
                    [alertControler addAction:[UIAlertAction actionWithTitle:@"レビューする" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        
                        NSURL *url = [NSURL URLWithString:URL_APP_STORE];
                        [[UIApplication sharedApplication]openURL:url];
                        [PLUtil saveBool:YES key:KEY_ENABLE_CHANGE_NOTIFI_MSG];
                    }]];
                    [self presentViewController:alertControler animated:YES completion:NULL];
                    
                }
            }else{
                //In review
                [self moveToMsgEditView];
            }
        }
        
    }else if (indexPath.section == _friendSection){
        //友達にすすめる
        [self shareToFriend];
    }
}

#pragma mark  MFMailComposeViewControllerDelegate

//メールを送信したあとの処理
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    if (result == MFMailComposeResultCancelled) {//
        //DEBUGLOG(@"メール送信キャンセル");
        
    }else if (result == MFMailComposeResultSaved) {//※Save Draftを押してから長い。（フリーズしたように思える）→くるくるがほしい
        //DEBUGLOG(@"保存完了");
        
    }else if (result == MFMailComposeResultSent) {
        //DEBUGLOG(@"送信完了");
        
    }else if (result == MFMailComposeResultFailed) {
        //DEBUGLOG(@"送信失敗or保存失敗");
        UIAlertView *alertFailed = [[UIAlertView alloc]
                                    initWithTitle:nil
                                    message:@"Failed"
                                    delegate:nil
                                    cancelButtonTitle:@"OK"
                                    otherButtonTitles:nil];
        [alertFailed show];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DEBUGLOG(@"index:%d",(int)buttonIndex);
    if (actionSheet.tag == PLActionSheetTagShareToFriend) {
        NSString *text = [NSString stringWithFormat:@"買い物し忘れ防止アプリ『%@』 %@",APP_NAME,URL_APP_STORE];
        if (buttonIndex == 0) {
            //LINE
            [self sendViaLine:text];
        }else if (buttonIndex == 1) {
            //Twitter
            [self sendViaTwitter:text];
        }else if (buttonIndex == 2) {
            //Facebook
            [self sendViaFacebook:text];
        } else  {
            //cancel
        }
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    }
}

@end
