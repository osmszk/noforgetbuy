//
//  main.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2014/12/26.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PLAppDelegate class]));
    }
}
