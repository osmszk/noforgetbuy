//
//  PLSelectViewController.m
//  LockMessage
//
//  Created by Osamu Suzuki on 2014/10/21.
//  Copyright (c) 2014年 Plegineer, Inc. All rights reserved.
//

#import "PLSelectViewController.h"

@interface PLSelectViewController ()
@property (nonatomic,assign) NSInteger selectIndex;
@property (nonatomic,strong) NSString *cellText;

@property (nonatomic,strong) NSMutableArray *counts;
@property (nonatomic,strong) NSArray *imageNames;
@end

@implementation PLSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //***UI***
    self.navigationController.navigationBar.translucent = NAVI_BAR_TRANSLUCENT;
    
    if (self.selectType == PLDSelectTypeRadius) {
        self.title = @"通知地点の半径";
        self.counts = [@[@"100m",@"200m",@"300m"]mutableCopy];
        NSInteger rad = [PLUtil loadInteger:KEY_RADIUS_NOTIFI];
        _selectIndex = [self selectIndexWithRaius:rad];
        if (_selectIndex<0) {
            _selectIndex=0;
        }
    }
}

- (NSInteger)selectIndexWithRaius:(NSInteger)rad
{
    switch (rad) {
        case 100:
            return 0;
        case 200:
            return 1;
        case 300:
            return 2;
        default:
            return 0;
    }
}

- (NSInteger)radiusWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return 100;
        case 1:
            return 200;
        case 2:
            return 300;
        default:
            return 100;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
#if ENABLE_ANALYTICS
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName value:@"Select Screen"];
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc
{
    LOG_CURRENT_METHOD;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_selectType == PLDSelectTypeRadius) {
        return [self.counts count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectType==PLDSelectTypeRadius){
        static NSString *identifier = @"selectcell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            cell.textLabel.minimumScaleFactor = 0.4f;
        }
        
        cell.textLabel.text = self.counts[indexPath.row];
        
        if (indexPath.row == _selectIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectType == PLDSelectTypeRadius) {
        return 44;
    }else{
        return 44;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (_selectType == PLDSelectTypeRadius) {
        return 48;
    }
    return 20;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (_selectType == PLDSelectTypeRadius) {
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 40)];
        textLabel.text = @"通知される領域（円形）の半径を設定できます。\nその領域に入ったら通知がされます。";
        textLabel.textColor = [UIColor lightGrayColor];
        textLabel.font = [UIFont systemFontOfSize:14];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.numberOfLines = 2;
        return textLabel;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectIndex = indexPath.row;
    [self.tableView reloadData];
    
    if (_selectType == PLDSelectTypeRadius) {
        NSInteger rad = [self radiusWithIndex:_selectIndex];
        [PLUtil saveInteger:rad key:KEY_RADIUS_NOTIFI];
    }
}

@end
