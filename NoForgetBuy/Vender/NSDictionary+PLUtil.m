//
//  NSDictionary+PLUtil.m
//  SoujiyaXLordNew
//
//  Created by Osamu Suzuki on 2014/01/16.
//  Copyright (c) 2014年 Plegineer, Inc. All rights reserved.
//

#import "NSDictionary+PLUtil.h"

@implementation NSDictionary (PLUtil)

- (id)objectForKeyConsideringNull:(id)key
{
    id object = [self objectForKey:key];
    if (object == [NSNull null]){
        return nil;
    }
    return object;
}

- (float)floatValueForKeyConsideringNull:(id)key;
{
    id object = [self objectForKey:key];
    if (object == [NSNull null]){
        return 0.0f;
    }
    return [object floatValue];
}

- (NSInteger)integerValueForKeyConsideringNull:(id)key
{
    id object = [self objectForKey:key];
    if (object == [NSNull null]){
        return 0.0f;
    }
    return [object integerValue];
}



@end
