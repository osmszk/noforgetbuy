//
//  NSDictionary+PLUtil.h
//  SoujiyaXLordNew
//
//  Created by Osamu Suzuki on 2014/01/16.
//  Copyright (c) 2014年 Plegineer, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PLUtil)

- (id)objectForKeyConsideringNull:(id)key;
- (float)floatValueForKeyConsideringNull:(id)key;
- (NSInteger)integerValueForKeyConsideringNull:(id)key;

@end
