//
//  FirstViewController.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2014/12/26.
//  Copyright (c) 2014年 Plegineer Inc. All rights reserved.
//

#import "PLMapViewController.h"
#import "PLNotificationHandler.h"

#import "NSDictionary+PLUtil.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "SVProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "ImobileSdkAds/ImobileSdkAdsIconParams.h"

@interface PLMapViewController ()<CLLocationManagerDelegate>
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL didInitializeUserLocation;
@property (nonatomic, strong) UIView *icon1View;
@property (nonatomic,weak) HTPressableButton *clearButton;
@end

@implementation PLMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //***UI***
    self.navigationController.navigationBar.translucent = NAVI_BAR_TRANSLUCENT;
    
    CGFloat TAB_H = 49.0f;
    CGFloat wAd = AD_ICON_W;
    CGFloat between = 8;
    CGFloat xAd = [PLUtil displaySize].width - wAd - between;
    CGFloat yAd = between + 20 + 44;
    UIView *adView1 = [[UIView alloc] initWithFrame:CGRectMake(xAd, yAd, wAd, wAd)];
    [self.view addSubview:adView1];
    self.icon1View = adView1;
    
    //広告
#if ENABLE_AD
    CGPoint p =  CGPointMake(0, [PLUtil displaySize].height-TAB_H-ceil(AD_BANNER_HIGHT * [PLUtil displaySize].width/320.0f));
    [ImobileSdkAds showBySpotID:AD_IMOBILE_SPOT_ID_BANNER1 ViewController:self Position:p SizeAdjust:YES];
#endif
    
    CGFloat x = 10;
    CGFloat w = [PLUtil displaySize].width - 2*x;
    CGFloat h = 50.0f;
    CGFloat betweenAd = 10.0f;
    CGFloat y = self.view.frame.size.height - h - TAB_H - ceil(AD_BANNER_HIGHT * [PLUtil displaySize].width/320.0f)-betweenAd;
    
    CGRect frame = CGRectMake(x, y, w, h);
    HTPressableButton *setButton = [[HTPressableButton alloc] initWithFrame:frame buttonStyle:HTPressableButtonStyleRounded];
    setButton.style = HTPressableButtonStyleRounded;
    setButton.buttonColor = [UIColor ht_grapeFruitColor];
    setButton.shadowColor = [UIColor ht_grapeFruitDarkColor];
    [setButton setTitle:@"この地点で通知させる！" forState:UIControlStateNormal];
    [setButton addTarget:self action:@selector(pushedSetButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setButton];
    
    
    CGFloat r = 26.0f;
    CGFloat cX = x;
    CGFloat cancelY = y - betweenAd - r*2;
    CGRect cancelFrame = CGRectMake(cX, cancelY, r*2,r*2);
    HTPressableButton *cancelButton = [[HTPressableButton alloc] initWithFrame:cancelFrame buttonStyle:HTPressableButtonStyleCircular];
    cancelButton.style = HTPressableButtonStyleCircular;
    cancelButton.buttonColor = [UIColor ht_sunflowerColor];
    cancelButton.shadowColor = [UIColor ht_citrusColor];
    [cancelButton setTitle:@"clear" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(pushedClearButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    self.clearButton = cancelButton;

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    
    //レビュー対策（ウォール広告）
    PLGAppVersionState appversionState = [PLUtil appVersionState];
    if (appversionState == PLGAppVersionStateFirst || appversionState == PLGAppVersionStateBumpedUp) {
        //サーバーチェック
        [PLUtil saveTrackingAppVersion];
        DEBUGLOG(@"first or bumpedUp!");
        [self startServerCheck];
        return;
    }
    
    if ([PLUtil loadBool:KEY_WALL_AD_SHOW_FLG]) {
        [self showIconAd];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
#if ENABLE_ANALYTICS
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName value:@"Map Screen"];
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif
}

#pragma mark - Custom Methods

- (void)showIconAd
{
#if ENABLE_AD
    ImobileSdkAdsIconParams *icon1Params = [[ImobileSdkAdsIconParams alloc] init];
    icon1Params.iconNumber = 1;
    icon1Params.iconTitleEnable = NO;
    icon1Params.iconSize = AD_ICON_W;
    icon1Params.iconViewLayoutWidth = AD_ICON_W;
    
    [ImobileSdkAds showBySpotID:AD_IMOBILE_SPOT_ID_ICON View:self.icon1View IconPrams:icon1Params];
#endif
}

- (void)showAlertViewAtInitial
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"確認(重要)" message:@"このアプリは、位置情報をもとにして通知するサービスです。登録した地点に移動すると通知します。\nこの次に表示される”通知の許可メッセージ”は、「許可」を押してくださいね。" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"わからない" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self showAlertViewAtInitial];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"わかった" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.locationManager requestWhenInUseAuthorization];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertLocationAuth
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"あ”ー！！！" message:@"だめです。位置情報を許可しないとこのアプリは使い物になりません。\n一度アプリを閉じて、設定>このアプリ>位置情報 から位置情報を許可してください。どうかお願いします。" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"わからない" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self showAlertLocationAuth];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"お、おう" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertSettingDone
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"設定完了！\nこの地点の近くを通ったときに通知がきます！" message:@"※GPSは誤差があるのである程度ずれる可能性があります。\n※すでにこの地点近くにいる場合は何もおこりません。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self addCircleOnMap];
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:NULL];
}

- (void)showAlertCancel
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"通知をクリアしました" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:NULL];
}

- (void)userLocationDidInitialize:(MKUserLocation *)userLocation
{
    NSArray *scheduledLocalNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    DEBUGLOG(@"scheduledLocalNotifications:%@",scheduledLocalNotifications);
    
    if ([scheduledLocalNotifications count] > 0)
    {
        // Notificationがスケジューリング済みならその領域を表示
        UILocalNotification *notification = scheduledLocalNotifications[0];
        CLLocationCoordinate2D center = [(CLCircularRegion *)notification.region center];
        MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:[PLUtil loadInteger:KEY_RADIUS_NOTIFI]];
        [self.mapView addOverlay:circle];
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(center, LOCATION_DISTANCE, LOCATION_DISTANCE) animated:YES];
    }else {
        // スケジューリングされてないなら現在地を表示
        CLLocationCoordinate2D center = userLocation.location.coordinate;
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(center, LOCATION_DISTANCE, LOCATION_DISTANCE) animated:YES];
        
    }
}

- (void)addCircleOnMap
{
    CLLocationCoordinate2D center = self.mapView.centerCoordinate;
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:[PLUtil loadInteger:KEY_RADIUS_NOTIFI]];
    [self.mapView addOverlay:circle];
}

- (void)cancelNotification
{
    NSArray *overlays = self.mapView.overlays;
    [self.mapView removeOverlays:overlays];
    [PLNotificationHandler cancelAllNotification];
}

- (void)startServerCheck
{
    if(![PLUtil isOnline]){
        return;
    }
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeClear];
    [self requestToCheckForReview];
}

- (void)requestToCheckForReview
{
    NSString *url = URL_REVIEW_API;
    
    ASIHTTPRequest *req = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
    [req setRequestMethod:@"GET"];
    [req setDidFinishSelector:@selector(requestFinished:)];
    [req setDidFailSelector:@selector(requestFailed:)];
    [req setDelegate:self];
    [req startAsynchronous];
}

- (BOOL)shouldBeNoWallAd:(NSString *)responseString
{
    if (responseString == nil || [responseString isEqual:[NSNull null]]) {
        return NO;
    }
    
    NSData *jsonData = [responseString dataUsingEncoding:NSUnicodeStringEncoding];
    
    NSError *error;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:NSJSONReadingAllowFragments
                                                              error:&error];
    if(error != nil){
        NSLog(@"convertDicFromJsonString error:%@",error);
        return NO;
    }
    
    float thisAppVer = [[PLUtil appVersionNumber] floatValue];
    float serverVer = [[PLUtil convertToNumberVersion:[jsonDic objectForKeyConsideringNull:@"version"]] floatValue];
    DEBUGLOG(@"sever:%f app:%f",serverVer,thisAppVer);
    BOOL isInReview = ([jsonDic integerValueForKeyConsideringNull:@"flg"]==1);
    if(thisAppVer > serverVer && isInReview){
        return YES;
    }
    return NO;
    
}

#pragma mark - Action

- (void)pushedSetButton:(id)sender
{
//    if ([self.mapView.overlays count] == 0) {
//        DEBUGLOG(@"mapView overlays 0!");
//        return;
//    }
    [self cancelNotification];
    
    CLLocationDirection radius = (float)[PLUtil loadInteger:KEY_RADIUS_NOTIFI];//通知エリア半径
    NSString *msg = [PLUtil loadObject:KEY_NOTIFI_MSG];

//    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(35.64669, 139.710106);//ebisu sta.
//    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(35.65239, 139.713097);//home
    CLLocationCoordinate2D loc = self.mapView.centerCoordinate;
    [PLNotificationHandler setNotificationWithTarget:loc radius:radius msg:msg];
    
    [self showAlertSettingDone];
}

- (void)pushedClearButton:(id)sender
{
    [self cancelNotification];
    
    [self showAlertCancel];
}

- (IBAction)pushedCurrentLocationButton:(id)sender
{
    CLLocationCoordinate2D center = self.mapView.userLocation.location.coordinate;
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(center, LOCATION_DISTANCE, LOCATION_DISTANCE) animated:YES];

}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (!self.didInitializeUserLocation)
    {
        self.didInitializeUserLocation = YES;
        [self userLocationDidInitialize:userLocation];
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKCircle class]]){
        MKCircleRenderer *renderer =
        [[MKCircleRenderer alloc] initWithCircle:(MKCircle *)overlay];
        renderer.strokeColor = [[UIColor redColor] colorWithAlphaComponent:0.4]; // 外側の線の色
        renderer.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.4]; // 内側を塗りつぶす色
        renderer.lineWidth = 1; // 外側の線の太さ
        
        return renderer;
    }
    return nil;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [manager requestWhenInUseAuthorization];
            NSLog(@"kCLAuthorizationStatusAuthorizedWhenInUse");
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            NSLog(@"kCLAuthorizationStatusAuthorizedAlways");
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            [self showAlertViewAtInitial];
            break;
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
            [self showAlertLocationAuth];
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
        default:
            
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIFormDataRequest *)request
{
    LOG_CURRENT_METHOD;
    
    [SVProgressHUD dismiss];
    NSString *jsonString    = [request responseString];
    DEBUGLOG(@"jsonDic:%@",jsonString);
    BOOL shouldNOWallAd = [self shouldBeNoWallAd:jsonString];
    
    if (shouldNOWallAd){
        [PLUtil saveBool:NO key:KEY_WALL_AD_SHOW_FLG];
    }else{
        [PLUtil saveBool:YES key:KEY_WALL_AD_SHOW_FLG];
        [self showIconAd];
    }
}

- (void)requestFailed:(ASIFormDataRequest *)request
{
    [SVProgressHUD dismiss];
    LOG_CURRENT_METHOD;
}

@end
