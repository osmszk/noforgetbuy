//
//  PLMsgEditViewController.m
//  NoForgetBuy
//
//  Created by 鈴木治 on 2015/01/03.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "PLMsgEditViewController.h"

#import "HTPressableButton.h"
#import "UIColor+HTColor.h"

@interface PLMsgEditViewController ()

@end

@implementation PLMsgEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //***UI***
    self.navigationController.navigationBar.translucent = NAVI_BAR_TRANSLUCENT;
    self.title = @"通知メッセージ設定";
    
    CGFloat x = 20;
    CGFloat w = [PLUtil displaySize].width - 2*x;
    CGFloat h = 44.0f;
    CGFloat y = 44+43+30+44+20;
    
    CGRect frame = CGRectMake(x, y, w, h);
    HTPressableButton *rectButton = [[HTPressableButton alloc] initWithFrame:frame buttonStyle:HTPressableButtonStyleRect];
    rectButton.style = HTPressableButtonStyleRect;
    rectButton.buttonColor = [UIColor ht_turquoiseColor];
    rectButton.shadowColor = [UIColor ht_greenSeaColor];
    [rectButton setTitle:@"決定" forState:UIControlStateNormal];
    [rectButton addTarget:self action:@selector(pushedDecideButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rectButton];
    
    _textField.text = [PLUtil loadObject:KEY_NOTIFI_MSG];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
#if ENABLE_ANALYTICS
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName value:@"MsgEdit Screen"];
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)pushedDecideButton:(id)sender
{
    //iOS7 107 character 全角53文字まで
    //ref:http://stackoverflow.com/questions/6307748/what-is-the-maximum-length-of-a-push-notification-alert-text
    
    if (_textField.text == nil || [_textField.text length] == 0) {
        [PLUtil showAlert:@"" message:@"文字がぬけてます"];
        return;
    }
    
    
    if ([_textField.text length]>53) {
        [PLUtil showAlert:@"" message:@"52文字以内でお願いします。"];
        return;
    }
    
    [PLUtil saveObject:_textField.text key:KEY_NOTIFI_MSG];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
