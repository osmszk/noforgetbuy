//
//  PLSelectViewController.h
//  LockMessage
//
//  Created by Osamu Suzuki on 2014/10/21.
//  Copyright (c) 2014年 Plegineer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PLDSelectType){
    PLDSelectTypeRadius = 0,
};

@interface PLSelectViewController : UITableViewController
@property (nonatomic,assign) PLDSelectType selectType;
@end
